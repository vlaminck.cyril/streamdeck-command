//
// Created by Cyril Vlaminck on 27/12/2021.
//

#include "ActionManager.h"

#include <utility>

#include "event/sent/LogMessageSentEvent.h"
#include "event/sent/ShowOkSentEvent.h"

using namespace openstreamdeck::internal::event;

namespace openstreamdeck::internal {

ActionManager::ActionManager(std::shared_ptr<Logger> logger, std::shared_ptr<CommunicationManager> communicationManager)
    : logger(std::move(logger)), communicationManager(std::move(communicationManager)) {
}

auto ActionManager::logMessage(std::string message) -> void {
    auto event = std::make_shared<LogMessageSentEvent>(std::move(message));
    this->sendEvent(event);
}

auto ActionManager::showOk(std::string context) -> void {
    auto event = std::make_shared<ShowOkSentEvent>(std::move(context));
    this->sendEvent(event);
}

auto ActionManager::sendEvent(std::shared_ptr<event::SentEvent> event) -> void {
    if (logger->isLogEnabled()) {
        logger->log(boost::format("Sending event '%1%'.") % event->event);
    }
    this->communicationManager->write(std::move(event));
}

}  // namespace openstreamdeck::internal
