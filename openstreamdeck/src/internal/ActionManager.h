//
// Created by Cyril Vlaminck on 27/12/2021.
//

#ifndef STREAMDECK_COMMAND_ACTIONMANAGER_H
#define STREAMDECK_COMMAND_ACTIONMANAGER_H

#include "CommunicationManager.h"
#include "Logger.h"
#include "StreamDeckPluginActions.h"
#include "event/sent/SentEvent.h"

namespace openstreamdeck::internal {

class ActionManager : public StreamDeckPluginActions {
   private:
    std::shared_ptr<Logger> logger;
    std::shared_ptr<CommunicationManager> communicationManager;

   public:
    ActionManager(std::shared_ptr<Logger> logger, std::shared_ptr<CommunicationManager> communicationManager);

    auto logMessage(std::string message) -> void override;

    auto showOk(std::string context) -> void override;

   private:
    auto sendEvent(std::shared_ptr<event::SentEvent> event) -> void;
};

}  // namespace openstreamdeck::internal

#endif  // STREAMDECK_COMMAND_ACTIONMANAGER_H
