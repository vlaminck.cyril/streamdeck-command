//
// Created by Cyril Vlaminck on 28/12/2021.
//

#ifndef STREAMDECK_COMMAND_SETTITLESENTEVENT_H
#define STREAMDECK_COMMAND_SETTITLESENTEVENT_H

#include "SentEvent.h"

namespace openstreamdeck::internal::event {

class SetTitleSentEvent : public SentEvent {
   public:
};

}  // namespace openstreamdeck::internal::event

#endif  // STREAMDECK_COMMAND_SETTITLESENTEVENT_H
