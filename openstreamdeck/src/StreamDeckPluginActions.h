//
// Created by Cyril Vlaminck on 27/12/2021.
//

#ifndef STREAMDECK_COMMAND_STREAMDECKPLUGINACTIONS_H
#define STREAMDECK_COMMAND_STREAMDECKPLUGINACTIONS_H

#include <string_view>

namespace openstreamdeck {

class StreamDeckPluginActions {
   public:
    virtual void logMessage(std::string message) = 0;

    virtual void showOk(std::string context) = 0;
};

}  // namespace openstreamdeck

#endif  // STREAMDECK_COMMAND_STREAMDECKPLUGINACTIONS_H
